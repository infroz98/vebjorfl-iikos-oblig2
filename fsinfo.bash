#!/bin/bash
readlink inn

path=$(find / 2>/dev/null $inn | grep $inn | head -1)

patisjonProsent=$(df $path | awk '{print $5}' | tail -1)
antallFiler=$(find $inn ! -wholename '*/.*' -type f -printf '%s %p\n' | wc -l)

folderSize=$(du -s $inn | awk '{print $1}')

maxFileSize=$(find $inn -not -path '*/\.*' -printf '%s %p\n' | sort -nr | head -1 )
maxFileSizeBytes=$(find $inn ! -wholename '*/.*' -type f -printf '%s %p\n' | sort -nr | head -1 |  awk '{print $1}')

p1=$(pwd)
p2=$(find $inn ! -wholename '*/.*' -type f -printf '%s %p\n' | sort -nr | head -1 |  awk '{print $2}')
pathStorst=$p1$p2

echo "Partisjon $inp er $patisjonProsent"
echo "Antall Filer: $antallFiler"

echo "Avg file size: $(( $folderSize / $antallFiler  )) B"
echo "Biggest File: $maxFileSize at $maxFileSizeBytes B  stor"
echo "Fil med flest hardlinks: $(find $inn -ls | sort -k 4,4 | tail -1 | awk '{print $11}' ), $(find $inn -ls | sort -k 4,4 | tail -1 | awk '{print $4}' )"
