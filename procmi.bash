#!/bin/bash

i=0

for a in "$@"; do
        commando[i]=$a
        fileName="${commando[i]}-$(date +%Y%m%d-%H:%M:%S).meminfo"
        touch $fileName

        echo "-> ${commando[i]}:" >> $fileName

        data=$(cat /proc/${commando[i]}/status | grep VmData | awk '{print $2}')
        stk=$(cat /proc/${commando[i]}/status | grep VmStk | awk '{print $2}')
        exe=$(cat /proc/${commando[i]}/status | grep VmExe | awk '{print $2}')
        sum=$((data+stk+exe))


        echo "vmSize: $(cat /proc/${commando[i]}/status | grep VmSize | awk '{print $2, $3}')" >> $fileName
        echo "pvmSize: $sum" >> $fileName
        echo "shared vmSize: $(cat /proc/${commando[i]}/status | grep VmLib | awk '{print $2, $3}')" >> $fileName
        echo "physical memory: $(cat /proc/${commando[i]}/status | grep VmRSS | awk '{print $2, $3}')" >> $fileName
        echo "Physical memory for page table: $(cat /proc/${commando[i]}/status | grep VmPTE | awk '{print $2, $3}')" >> $fileName
        ((i++))
done

echo "$i"
