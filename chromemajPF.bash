#!/bin/bash
pids=$(pgrep chrome)

for pid in $pids;
do
  p=$(ps --no-headers -o maj_flt $pid)

  if [[ $p -lt 1000 ]];
    then
      echo "$pid major faults: $p"
  else
    echo "$pid har skapt mer enn 1000 faults ($p)"
  fi
done
